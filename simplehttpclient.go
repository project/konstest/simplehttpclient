package simplehttpclient

import (
	"context"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

type Client struct {
	retries               int           // number of retries, default is 5
	correctHttpStatus     int           // the HTTP status code that must be returned by http request, HTTP 200 by default
	ignoreCheckHttpStatus bool          // set the value to [true] if you need to ignore HTTP status code check, default is [false]
	retriesTimeout        time.Duration // retries timeout, default is 1 second
	httpClient            *http.Client  // if nil will be used default http client
}

/*
NewClient returns a new simple http client.

If [httpClient] is nil then will be used the default http client.
*/
func NewClient(httpClient *http.Client) *Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
		httpClient.Timeout = 30 * time.Second
	}
	return &Client{
		retries:               5,
		correctHttpStatus:     http.StatusOK,
		ignoreCheckHttpStatus: false,
		retriesTimeout:        1 * time.Second,
		httpClient:            httpClient,
	}
}

// Do makes an http request and returns its response
func (c *Client) Do(ctx context.Context, method, url string, requestBody io.Reader, headers map[string]string) (*http.Response, error) {
	dLog := log.WithField(method+" url", safeUrlPrint(url))

	var resp *http.Response

	method = strings.ToUpper(method)
	switch method {
	case http.MethodGet,
		http.MethodHead,
		http.MethodPost,
		http.MethodPut,
		http.MethodPatch,
		http.MethodDelete,
		http.MethodConnect:
	default:
		return nil, fmt.Errorf("unsupported http method: %s", method)
	}

	dLog.Debugln("starting http request")
	// construct http request
	req, err := http.NewRequestWithContext(ctx, method, url, requestBody)
	if err != nil {
		dLog.WithError(err).Error("can't construct http request")
		return nil, err
	}

	// set http headers
	for h, v := range headers {
		req.Header.Set(h, v)
	}

	for i := 0; i < c.retries; i++ {
		dLog.Debugf("Try to execute request. Attempt %d.", i+1)
		if i > 0 {
			time.Sleep(c.retriesTimeout) // wait before retrying
		}

		resp, err = c.httpClient.Do(req)

		if err != nil {
			dLog.Error(err)
			continue
		}

		if c.ignoreCheckHttpStatus {
			break
		}

		if resp.StatusCode == c.correctHttpStatus {
			break
		} else {
			err = fmt.Errorf("http %d != http %d", c.correctHttpStatus, resp.StatusCode)
			dLog.Error(err)
		}
	}
	dLog.Debugln("http request completed")
	return resp, err
}

// Download uploads the contents of the request body to the specified local file
func (c *Client) Download(ctx context.Context, url, dstLocalFilePath string, permissions fs.FileMode) error {
	dLog := log.WithField("GET url", safeUrlPrint(url)).WithField("dst file", dstLocalFilePath)

	// Check that the file exists
	dstDir := filepath.Dir(dstLocalFilePath)
	_, err := os.Stat(dstLocalFilePath)
	if errors.Is(err, os.ErrNotExist) {
		// try to create folder and dst file before starting download process
		err = os.MkdirAll(dstDir, fs.ModeDir|0o755)
		if err != nil {
			dLog.WithError(err).Error("failed to create a folder")
			return err
		}
		_, err := os.Create(dstLocalFilePath)
		if err != nil {
			dLog.WithError(err).Error("failed to create a file")
			return err
		}
		// removing the newly created empty file
		_ = os.Remove(dstLocalFilePath)
	} else if err != nil {
		dLog.WithError(err).Error("dst file check error")
		return err
	}

	dLog.Debugln("The download is starting")
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		dLog.WithError(err).Error("can't construct http request")
		return err
	}

	tmpFilePath := fmt.Sprintf("%s_%s", dstLocalFilePath, randomString(16))
	var (
		resp *http.Response
		t    *os.File
	)
	for i := 0; i < c.retries; i++ {
		if i == c.retries-1 {
			return err
		}
		if i > 0 {
			time.Sleep(c.retriesTimeout) // wait before retrying
		}
		dLog.Debugf("Try to download file. Attempt %d.", i+1)

		resp, err = c.httpClient.Do(req)
		if err != nil {
			dLog.WithError(err).Error("error happend during http request")
			continue
		}

		if !c.ignoreCheckHttpStatus {
			if resp.StatusCode != c.correctHttpStatus {
				err = fmt.Errorf("http %d != http %d", c.correctHttpStatus, resp.StatusCode)
				dLog.Error(err)
				continue
			}
		}

		t, err = os.Create(tmpFilePath)
		if err != nil {
			dLog.WithError(err).WithField("tmp file", tmpFilePath).Error("failed to create a tmp file")
			return err
		}

		err = os.Chmod(tmpFilePath, permissions)
		if err != nil {
			return fmt.Errorf("cat't set permissions for the file %s: %w", tmpFilePath, err)
		}

		_, err = CopyByChunks(ctx, resp.Body, t, 1024)
		if err != nil {
			dLog.WithError(err).Warnln("can't save the downloading file to disk")
			continue
		}

		if err := resp.Body.Close(); err != nil {
			dLog.WithError(err).Warnln("can't close response body")
		}
		if err := t.Close(); err != nil {
			dLog.WithError(err).Warnln("can't close temp file")
		}

		needMoveTmpfileToDst := true
		if _, err := os.Stat(dstLocalFilePath); err == nil {
			dLog.Debugln("the downloading file already exists")
			needMoveTmpfileToDst = false

			f, err := os.Open(dstLocalFilePath)
			if err != nil {
				dLog.WithError(err).Error("can't open the existing destination file")
				return err
			}

			dLog.Debugln("calculating md5sum of existing file")
			existFileMD5sum, err := MD5sum(ctx, f)
			if err != nil {
				dLog.Error(err)
				return err
			}
			if err := f.Close(); err != nil {
				dLog.Warnln(err)
			}
			// compare the md5sum
			if err := CheckMD5sum(ctx, tmpFilePath, existFileMD5sum); err != nil {
				// if the md5sum of the existing file and the downloaded file do not match, we need to rewrite it.
				needMoveTmpfileToDst = true
			}
		}

		if needMoveTmpfileToDst {
			// move the temprorary downloaded file to dstLocalFilePath
			if err := os.Rename(tmpFilePath, dstLocalFilePath); err != nil {
				dLog.WithError(err).WithField("tmp file", tmpFilePath).Error("can't rename the tmp file to dest filename")
				return err
			}
		} else {
			// dst file already downloaded, deleting the temporary file
			err = os.Remove(tmpFilePath)
			if err != nil {
				dLog.WithError(err).WithField("tmp file", tmpFilePath).Warnln("can't remove the temp file")
			}
		}

		break // the file was successfully downloaded, exit from the loop
	}

	dLog.Debugln("The download is complete")
	return nil
}

// SetRetries sets the number of retries, default is 5
func (c *Client) SetRetries(count int) *Client {
	c.retries = count
	return c
}

// SetCorrectHttpStatus sets the HTTP status code that must be returned by http request, HTTP 200 by default
func (c *Client) SetCorrectHttpStatus(httpStatus int) *Client {
	c.correctHttpStatus = httpStatus
	return c
}

// SetIgnoreCheckHttpStatus set the value to [true] if you need to ignore HTTP status code check, default is [false]
func (c *Client) SetIgnoreCheckHttpStatus(b bool) *Client {
	c.ignoreCheckHttpStatus = b
	return c
}

// SetRetriesTimeout sets timeout between retries, default is 1 second
func (c *Client) SetRetriesTimeout(timeout time.Duration) *Client {
	c.retriesTimeout = timeout
	return c
}

/*
Converts urls for correct output. Example:

	https://user:pass@company.com/foo/bar to company.com/foo/bar
*/
func safeUrlPrint(url string) string {
	d := strings.SplitAfterN(url, "@", 2)
	result := d[0]
	if len(d) > 1 {
		result = d[1]
	}
	return result
}

/*
RandomString generates a random string of length "n"
*/
func randomString(n int) string {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[r.Intn(len(letterRunes))]
	}
	return string(b)
}
