package simplehttpclient

import (
	"context"
	"fmt"
	"io"
)

/*
Copying the content in parts from reader to specified writer, with Golang context.
*/
func CopyByChunks(ctx context.Context, src io.Reader, dst io.Writer, readBuffer int) (nBytes int64, err error) {
	buf := make([]byte, readBuffer)
	for {
		select {
		case <-ctx.Done():
			return nBytes, fmt.Errorf("the copying process was interrupted")
		default:
		}
		n, err := src.Read(buf)
		if err != nil && err != io.EOF {
			nBytes += int64(n)
			return nBytes, fmt.Errorf("an error occurred during the reading process: %w", err)
		}
		if n == 0 {
			break
		}

		if _, err := dst.Write(buf[:n]); err != nil {
			return nBytes, fmt.Errorf("an error occurred during the writing process: %w", err)
		}
		nBytes += int64(n)
	}
	return nBytes, nil
}
