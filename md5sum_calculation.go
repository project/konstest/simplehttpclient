package simplehttpclient

import (
	"context"
	"crypto/md5"
	"fmt"
	"io"
	"os"

	log "github.com/sirupsen/logrus"
)

/*
Calculates the md5sum of the specified file and compares it with the provided one.
*/
func CheckMD5sum(ctx context.Context, pathToFile, requiredMD5sum string) error {
	dLog := log.WithField("checkMD5sum", pathToFile)
	f, err := os.Open(pathToFile)
	defer f.Close()
	if err != nil {
		return fmt.Errorf("can't open file: %w", err)
	}
	dLog.Debugln("md5sum calculation started")
	calculatedMD5sum, err := MD5sum(ctx, f)
	if err != nil {
		return err
	}
	dLog.Debugf("the calculated md5sum is %s", calculatedMD5sum)
	if calculatedMD5sum != requiredMD5sum {
		return fmt.Errorf("the calculated md5sum %s does not match to required md5sum '%s'", calculatedMD5sum, requiredMD5sum)
	}
	return nil
}

// MD5Sum calculates md5 digest of the given content
func MD5sum(ctx context.Context, file io.Reader) (string, error) {
	h := md5.New()
	_, err := CopyByChunks(ctx, file, h, 1024)
	if err != nil {
		return "", fmt.Errorf("md5sum error: %w", err)
	}
	return fmt.Sprintf("%x", h.Sum(nil)), nil
}
