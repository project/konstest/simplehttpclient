package simplehttpclient

import (
	"context"
	"net/http"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestFileDownload(t *testing.T) {
	// log.SetLevel(log.DebugLevel)
	url := "https://download.freebsd.org/releases/amd64/amd64/ISO-IMAGES/14.1/FreeBSD-14.1-RELEASE-amd64-bootonly.iso.xz"
	tmpDownloadDir := "tmpDownloadDir"
	destFile := filepath.Join(tmpDownloadDir, filepath.Base(url))
	defer os.RemoveAll(tmpDownloadDir)

	downloader := NewClient(nil).SetRetries(7)

	// first download
	require.NoError(t, downloader.Download(context.Background(), url, destFile, 0o644))
	firstDownloadedFile, err := os.Stat(destFile)
	require.NoError(t, err)

	// second download
	require.NoError(t, downloader.Download(context.Background(), url, destFile, 0o644))
	secondDownloadedFile, err := os.Stat(destFile)
	require.NoError(t, err)

	// checking that the file has not been replaced when redownloading
	require.True(t, firstDownloadedFile.ModTime() == secondDownloadedFile.ModTime())
}

func TestDoRequests(t *testing.T) {
	// log.SetLevel(log.DebugLevel)
	client := NewClient(nil).SetRetries(7)
	r, err := client.Do(context.Background(), http.MethodGet, "https://ya.ru", nil, nil)
	require.NoError(t, err)
	require.NotNil(t, r)

	m := make(map[string]string)
	m["Content-Type"] = "application/json"
	r, err = client.Do(context.Background(), http.MethodGet, "https://ya.ru", nil, m)
	require.NoError(t, err)
	require.NotNil(t, r)
}
